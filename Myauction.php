<?php
session_start();
include 'data_access_helper.php';
$db = new DataAccessHelper;
$db->connect();
mysqli_set_charset($conn, 'UTF8');


//add item in shopping cart
if(isset($_POST["type"]) && $_POST["type"]=='add')
{
  $PD_ID   = filter_var($_POST["PD_ID"], FILTER_SANITIZE_STRING); //product code
  

  //MySqli query - get details of item from db using product code
  $results = mysqli_query($conn,"SELECT Name,Price,Content,Image_link FROM product WHERE PD_ID='$PD_ID' LIMIT 1");
  $obj = mysqli_fetch_object($results);
  
  if ($results) { //we have the product info 
    
    //prepare array for the session variable
    $new_product = array(array('Name'=>$obj->Name, 'PD_ID'=>$PD_ID ,'Content'=>$obj->Content,'Image_link'=>$obj->Image_link, 'Price'=>$obj->Price));
    
    if(isset($_SESSION["product"])) //if we have the session
    {
      $found = false; //set found item to false
      
      foreach ($_SESSION["product"] as $cart_itm) //loop through session array
      {
        if($cart_itm["PD_ID"] == $PD_ID){ //the item exist in array

          $product[] = array('Name'=>$cart_itm["Name"], 'PD_ID'=>$cart_itm["PD_ID"],  'Price'=>$cart_itm["Price"],  'Content'=>$cart_itm["Content"],  'Image_link'=>$cart_itm["Image_link"]);
          $found = true;
        }else{
          //item doesn't exist in the list, just retrive old info and prepare array for session var
          $product[] = array('Name'=>$cart_itm["Name"], 'PD_ID'=>$cart_itm["PD_ID"],  'Price'=>$cart_itm["Price"],  'Content'=>$cart_itm["Content"],  'Image_link'=>$cart_itm["Image_link"]);
        }
        
      }
      
      if($found == false) //we didn't find item in array
      {
        //add new user item in array
        $_SESSION["product"] = array_merge($product, $new_product);
      }else{
        //found user item in array list, and increased the quantity
        $_SESSION["product"] = $product;
      }
      
    }else{
      //create a new session var if does not exist
      $_SESSION["product"] = $new_product;
    }
  
  }

}


$db->close();
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="text/css" href="img/favicon.ico">

    <title>Aladdin - Sàn đấu giá sản phẩm tốt</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>

    <!-- Custom styles for this template -->
    <link type="text/css" href="css/header.css" rel="stylesheet">
    <link type="text/css" href="css/rating.css" rel="stylesheet">
    <link type="text/css" href="css/Myauction.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="css/style.css" />

  </head>
  <body>


    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="homepage.php"><img src="img/brand-logo.png" width="220" height="78px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

    <!-- Search -->
    <div id="main" >   
      <div class="main-search">   
          <div class="input-group">
            <form class="form-inline" action="Search.php" method="GET">

              <input type="search"   class="form-control " placeholder="Tìm kiếm..." name="q">
              <div class="input-group-btn">
              <button class="btn btn-default" type="submit" name="search" ><i class="fas fa-search"></i></button>
              </div>              
            </form>
          </div>  
          <br>
        <!-- Grid system -->
        <div id="search-result" class="row"> 
        </div>
      </div>    
    </div>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="shopping_cart.php"><i class="fas fa-shopping-cart"></i> Giỏ hàng
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="Myauction.php"> Đấu giá của tôi <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fas fa-user"></i> Tài khoản của tôi</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    

    <!-- Page Content -->
    <div style="margin-top: 2%" class="container">

      <div class="row"  style="padding-top: 2%">

      
              <div class="col-md-12">
            <div class="product-tab">

              <div class="tabs">
                <ul class="tab-links">
                  <li class="active"><a href="#tab1">Giá thầu hiện tại</a></li>
                  <li><a href="#tab2">Giá thầu đã thắng</a></li>
                  <li><a href="#tab3">Giá thầu đã thua</a></li>
                  
                </ul>

              <div class="tab-content">
                <div id="tab1" class="tab active" style="padding-top: 10px">
                    <div class="container"> 
                     <table id="cart" class="table table-hover table-condensed"> 
                      <thead> 
                       <tr> 
                        <th style="width:40%">Tên sản phẩm</th> 
                        <th style="width:10%">Giá thầu của bạn</th> 
                        <th style="width:15%">Giá thầu tối thiểu hiện tại</th> 
                        <th style="width:15%" class="text-center">Người đấu giá cao nhất</th>
                        <th style="width:20%" class="text-center">Hoàn thành</th>
                       </tr> 
                      </thead> 
                      <tbody>
                        <?php
                      
                      if(isset($_SESSION["product"]))
                      {
                          foreach ($_SESSION["product"] as $cart_itm)
                          {
                            echo '<tr>';
                           echo '<td data-th="Product">'; 
                           echo '<div class="row">';

                            echo "<div class=col-sm-3 hidden-xs><img src=".$cart_itm["Image_link"]." width=100/>";
                            echo '</div>'; 
                            echo '<div class="col-sm-9">'; 

                            echo '<h5 class="nomargin">'.$cart_itm["Name"].'</h5>';
                            $PD_ID = $cart_itm["PD_ID"]; 
                            echo "<a href = 'product-detail.php?PD_ID=$PD_ID' style='color:blue' >Phiên đấu: $PD_ID </a>";
                            echo '</div>';

                            echo '</div>'; 
                           echo '</td>'; 
                           echo '<td data-th="Price" class="text-center">'.$cart_itm["Price"].'</td>'; 
                           
                           echo '<td data-th="Subtotal" class="text-center">'.$cart_itm["Price"].'</td>';
                           echo '<td data-th="Subtotal" class="text-center">'.$cart_itm["Price"].'</td>';
                           
                           echo '</tr>';

                           }
                           }else{
                          echo 'Your Cart is empty';
                          } 
                       ?>
                      
                      
                      </tbody>
                      </table>
                    </div>
                </div>
                <div id="tab2" class="tab" style="padding-top: 10px">
                    <div class="container"> 
                     <table id="cart" class="table table-hover table-condensed"> 
                      <thead> 
                       <tr> 
                        <th style="width:40%">Tên sản phẩm</th> 
                        <th style="width:10%">Giá thầu của bạn</th> 
                        <th style="width:15%">Giá thầu tối đa của bạn</th> 
                        <th style="width:15%" class="text-center">Người đấu giá cao nhất</th>
                        <th style="width:20%" class="text-center">Hoàn thành</th> 
                       </tr> 
                      </thead> 
                      <tbody>
                        <?php
                      
                      if(isset($_SESSION["product"]))
                      {
                          foreach ($_SESSION["product"] as $cart_itm)
                          {
                            echo '<tr>';
                           echo '<td data-th="Product">'; 
                           echo '<div class="row">';

                            echo "<div class=col-sm-3 hidden-xs><img src=".$cart_itm["Image_link"]." width=100/>";
                            echo '</div>'; 
                            echo '<div class="col-sm-9">'; 

                            echo '<h5 class="nomargin">'.$cart_itm["Name"].'</h5>'; 
                            
                            echo '</div>';

                            echo '</div>'; 
                           echo '</td>'; 
                           echo '<td data-th="Price">'.$cart_itm["Price"].'</td>'; 
                           echo '</td>'; 
                           echo '<td data-th="Subtotal" class="text-center">'.$cart_itm["Price"].'</td>';
                           
                           }
                           }else{
                          echo 'Your Cart is empty';
                          } 
                       ?>
                      </tr>
                      
                      </tbody>
                      </table>
                    </div>
                </div>

                <div id="tab3"class="tab" style="padding-top: 10px">

                  <div class="container"> 
                     <table id="cart" class="table table-hover table-condensed"> 
                      <thead> 
                       <tr> 
                        <th style="width:40%">Tên sản phẩm</th> 
                        <th style="width:10%">Giá thầu của bạn</th> 
                        <th style="width:15%">Giá thầu cao nhất cuối cùng</th> 
                        <th style="width:15%" class="text-center">Người đấu giá cao nhất</th> 
                        <th style="width:20%" class="text-center">Hoàn thành</th>
                       </tr> 
                      </thead> 
                      <tbody>
                        <?php
                      
                      if(isset($_SESSION["product"]))
                      {
                          foreach ($_SESSION["product"] as $cart_itm)
                          {
                            echo '<tr>';
                           echo '<td data-th="Product">'; 
                           echo '<div class="row">';

                            echo "<div class=col-sm-3 hidden-xs><img src=".$cart_itm["Image_link"]." width=100/>";
                            echo '</div>'; 
                            echo '<div class="col-sm-9">'; 

                            echo '<h5 class="nomargin">'.$cart_itm["Name"].'</h5>'; 
                            
                            echo '</div>';

                            echo '</div>'; 
                           echo '</td>'; 
                           echo '<td data-th="Price">'.$cart_itm["Price"].'</td>'; 
                           echo '</td>'; 
                           echo '<td data-th="Subtotal" class="text-center">'.$cart_itm["Price"].'</td>';
                           
                           }
                           }else{
                          echo 'Your Cart is empty';
                          } 
                       ?>
                      </tr>
                      
                      </tbody>
                      </table>
                    </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
              <!-- /.col-lg-6  -->
            </div>
              <!-- /.row  -->
          </div>
          <!-- /.container  -->

    <!-- Footer -->
    <footer class="py-5">
      <div class="container">
        <div class="row">

          <div class="col-lg-4">
            <h4 class="my-4">Về Aladdin</h4>
            <br><a href="#">Về chúng tôi</a></br>
            <br><a href="#">Điều khoản sử dụng</a></br>
            <br><a href="#">Chính sách bảo mật</a></br>
            <br><a href="#">Thông tin công ty</a></br>
            <br><a href="#">Tuyển dụng</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Trợ Giúp & Liên Hệ</h4>
            <br><a href="#">Liên hệ</a></br>
            <br><a href="#">Thông tin giao hàng</a></br>
            <br><a href="#">Đăng ký</a></br>
            <br><a href="#">Trợ giúp</a></br>
            <br><a href="#">Thanh toán & Vận chuyển</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Liên Kết</h4>
            <br><a href="#"><i class="fab fa-facebook"></i> Facebook</a></br>
            <br><a href="#"><i class="fab fa-twitter"></i> Twitter</a></br>
            <br><a href="#"><i class="fab fa-instagram"></i> Instagram</a></br>
            <br><a href="#"><i class="fab fa-youtube"></i> Youtube</a></br>
          </div>

      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/clock.js" ></script>
    <script type="text/javascript" src="js/box-auction.js" ></script>
    <script src="js/tab.js"></script>
    <script type="text/javascript" src="js/Time-end.js" ></script>
    
    
    <!-- <script type="text/javascript" src="thử.js" ></script> -->
    <script type="text/javascript">
      $('document').ready(function() {
        $('.carousel').carousel();
      })
    </script>


  </body>
</html>

