<?php include('data_access_helper.php');?>
<?php
 $errors=array();
//connect to database
$db = new DataAccessHelper();
$db-> connect();

 //check user in login page
if (isset($_POST['login'])){	//login là name của button
	$username = isset($_POST['username']) ? $_POST['username'] : '';
  	$password = isset($_POST['password']) ? $_POST['password'] : '';
	
	//ensure that form field are filled property
	  if (empty($username)){
		array_push($errors, "Tên cần được nhập");
	  }
	  if (empty($password)){
		array_push($errors, "Password cần được nhập");
		}
	// check users in database to login
	  if (count($errors) ==0){
		$password = md5($password);//encrypt the password before storing in database
		$query = "SELECT* FROM users  WHERE username='$username' AND password='$password'";
		$result=mysqli_query($GLOBALS['conn'],$query);
		if(mysqli_num_rows($result)== 1){	
			$_SESSION['username']=$username;		
			header('location: homepage.php');//redirect to the homepage
		}
	  else{
		  array_push($errors,"Tên hoặc password bạn nhập đã sai");
		}
	}
}
$db-> close();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
  </head>
  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header" align="center">Đăng Nhập</div>
        
        <div class="card-body">
       
          <form method="post" action="login.php">
         
            <div class="form-group">
          	<?php include('errors.php');?>
              <div class="form-label-group">
                <input type="text" id="username" name="username" class="form-control" placeholder="Email address">
                <label for="username">Họ và Tên</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password">
                <label for="inputPassword">Mật Khẩu</label>
              </div>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me">
                  Nhớ mật khẩu của bạn
                </label>
              </div>
            </div>
            <button type="submit" name="login" class="btn btn-primary btn-block" >Đăng Nhập</button>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="register.php">Đăng kí tài khoản</a>
            <a class="d-block small" href="forgot-password.html">Quên mật khẩu?</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  </body>
</html>
