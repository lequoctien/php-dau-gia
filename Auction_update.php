<?php
session_start();
 include 'data_access_helper.php';
 ?> 

<?php

 //start session
$db = new DataAccessHelper;
$db->connect();
mysqli_set_charset($conn, 'UTF8');


//empty cart by distroying current session
if(isset($_GET["emptycart"]) && $_GET["emptycart"]==1)
{
    $return_url = base64_decode($_GET["return_url"]); //return url
    session_destroy();
    header('Location:'.$return_url);
}
 
//add item in shopping cart
if(isset($_POST["type"]) && $_POST["type"]=='add')
{
    $Category   = filter_var($_POST["Category"], FILTER_SANITIZE_STRING);
    $return_url     = base64_decode($_POST["return_url"]);
    
    $results = mysqli_query("SELECT Name,Price FROM product WHERE Category='$Category' LIMIT 1");
    $obj = mysqli_fetch_object($results);
    
    if ($results) { 

        $new_product = array(array( 'Category'=>$Category, 'Name'=>$obj->Name, 'Price'=>$obj->Price));
        
        if(isset($_SESSION["product"])) 
        {
            $found = false; 
            
            foreach ($_SESSION["product"] as $cart_itm)
            {
                if($cart_itm["Category"] == $Category){
 
                    $product[] = array('Name'=>$cart_itm["Name"], 'Category'=>$cart_itm["Category"], 'Price'=>$cart_itm["Price"]);
                    $found = true;
                }
            }
            
            if($found == false) //we didn't find item in array
            {
                //add new user item in array
                $_SESSION["product"] = array_merge($product, $new_product);
            }else{
                //found user item in array list, and increased the quantity
                $_SESSION["product"] = $product;
            }
            
        }else{
            //create a new session var if does not exist
            $_SESSION["product"] = $new_product;
        }
        
    }
    
    //redirect back to original page
    header('Location:'.$return_url);
}
 
//remove item from shopping cart
if(isset($_GET["removep"]) && isset($_GET["return_url"]) && isset($_SESSION["product"]))
{
    $Category   = $_GET["removep"]; //get the product code to remove
    $Image_link     = base64_decode($_GET["return_url"]); //get return url
 
    
    foreach ($_SESSION["product"] as $cart_itm) //loop through session array var
    {
        if($cart_itm["Category"]!=$Category){ //item does,t exist in the list
            $product[] = array('Name'=>$cart_itm["Name"], 'Category'=>$cart_itm["Category"], 'Price'=>$cart_itm["Price"]);
        }
        
        //create a new product list for cart
        $_SESSION["product"] = $product;
    }
    
    //redirect back to original page
    header('Location:'.$return_url);
}

$db->close();
?>

