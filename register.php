<?php 
include('data_access_helper.php');
?>

<?php
$username ="";
 $email="";
 $errors=array();
//connect to database
$db = new DataAccessHelper();
$db-> connect();

if(isset($_POST['register'])){
  $username = isset($_POST['username']) ? $_POST['username'] : '';
  $email = isset($_POST['email']) ? $_POST['email'] : '';
  $password_1 = isset($_POST['password_1']) ? $_POST['password_1'] : '';
  $password_2 = isset($_POST['password_2']) ? $_POST['password_2'] : '';


 //ensure that form field are filled property
  if (empty($username)){
    array_push($errors, "Tên cần được nhập");
  }
  if (empty($email)){
    array_push($errors, "Email cần được nhập");
  }
  if (empty($password_1)){
    array_push($errors, "Mật khẩu cần được nhập");
  }
  if ($password_1 != $password_2){
    array_push($errors, "Hai mật khẩu không trùng khớp");
  }
  //if there are no errors, save users to database
  if (count($errors) ==0){
    $password = md5($password_1);//encrypt the password before storing in database
    $sql= "INSERT INTO users (username, email, password) VALUES ('$username', '$email', '$password')";
    mysqli_query($GLOBALS['conn'],$sql);   
    header('location: login.php');//redirect to the homepage
  }
 }
$db-> close();
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header" align="center">Đăng Kí</div>
        <div class="card-body">
          <form method="post" action="register.php">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <?php include('errors.php');?>
                  <div class="form-label-group">
                    <!--Hiển thị errors ở đây-->
                  
                    <input type="text" id="username" name="username" class="form-control" placeholder="username" value="<?php echo$username?>">
                    <label for="username">Họ và Tên</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="<?php echo $email?>">
                <label for="inputEmail">Địa chỉ Email của bạn</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" name="password_1" class="form-control" placeholder="Password">
                    <label for="inputPassword">Mật khẩu</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="confirmPassword" name="password_2" class="form-control" placeholder="Confirm password">                    <label for="confirmPassword">Nhập lại mật khẩu</label>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block" name="register">Đăng Kí</button>
          </form>
			<div class="text-center">
            <a class="d-block small mt-3" name="login" href="login.php">Đăng nhập</a>
            </div>
            <!--
            <a class="d-block small" href="forgot-password.html">Quên mật khẩu?</a>
          	-->
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  </body>

</html>
