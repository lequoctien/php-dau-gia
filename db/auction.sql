-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 17, 2019 at 01:54 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auction`
--

-- --------------------------------------------------------

--
-- Table structure for table `myauction`
--

DROP TABLE IF EXISTS `myauction`;
CREATE TABLE IF NOT EXISTS `myauction` (
  `Username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Myprice` int(255) NOT NULL,
  `PD_ID` int(255) NOT NULL,
  `Highprice` int(255) NOT NULL,
  `Auction_time` timestamp NOT NULL,
  `P_Content` text COLLATE utf8_unicode_ci NOT NULL,
  `P_Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `P_Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `PD_ID` int(255) NOT NULL AUTO_INCREMENT,
  `Category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Price` int(255) NOT NULL,
  `Content` text COLLATE utf8_unicode_ci NOT NULL,
  `Image_link` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PD_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`PD_ID`, `Category`, `Name`, `Price`, `Content`, `Image_link`) VALUES
(1, 'Áo thun', 'Áo thun nam tay dài', 212000, 'Áo Thun Nam Tay Dài được thiết kế cổ tròn, tay dài phối với nhiều màu tạo nên sự trẻ trung, năng động cập nhật xu hướng thời trang mới cho các bạn trẻ. Tay dài, phối màu sắc trắng đen chủ đạo giúp áo thêm điểm nhấn trẻ trung phù hợp để kết hợp với quần jean, kaki dài để đi học hay quần short, quần lửng để đi chơi, dạo phố cùng gia đình và bạn bè. Kết hợp thêm một chiếc áo khoác nỉ là bạn có thể lên đường cho một chuyến du lịch rồi đấy.', 'https://media3.scdn.vn/img2/2018/6_6/ao-thun-nam-tay-dai-1m4G3-g88114_simg_d0daf0_800x1200_max.jpg'),
(2, 'Áo sơ mi', 'Áo sơ mi nam kate tay dài thời trang QI227', 100000, ' Áo sơ mi nam kate tay dài thời trang, chất liệu vải lụa mịn, co dãn nhẹ, hàng cao cấp chuẩn.', 'http://www.qidi.vn/files/sanpham/28/1/jpg/ao-so-mi-nam-kate-tay-dai-thoi-trang-qi227.jpg'),
(3, 'Áo khoác', 'Áo khoác nam', 100000, 'Áo khoác dù namcó nón dường như đã quá quen thuộc đối với người dân Việt Nam. Nổi tiếng với chất vải bền, đẹp, đáng tiền.\r\n- Chất liệu vải dù dày dặn giúp bảo vệ làn da của bạn tránh khỏi tác động của ánh nắng mặt trời mà không bị các bệnh về da.\r\n- Áo khoác của xưởngchúng tôi sản xuất ra luôn được đảm bảo về các tiêu chuẩn chất lượng, qua các công đoạn kiểm tra nghiêm ngặt để tạo ra sản phẩm nên các bạn không cần lo lắng về sản phẩm.\r\n- Với chất liệu dù cao cấp cho bạn một chiếc áo khi mặc vào tôn lên vẻ nam tính, trẻ trung.\r\n- Áokhoác nam được bán ra đảm bảo giặt sẽkhông ra màu, không xù lôngtheo thời gian\r\n- Áo có tổng cộng ba màu tha hồ cho các bạn lựa chọn. Lấy ngay ba cáiđể mặccả tuần mà không cần phải đắn đo suy nghĩ rằng ngày mai mặc gì', 'https://vn-live-01.slatic.net/original/2f2cdb8fc0573e7dfbc14397f546d919.jpg'),
(4, 'Máy ảnh', 'Máy ảnh EOS 1100D (Kit)', 1000000, 'EOS 1100D là một chiếc máy ảnh thu hút thêm người sử dụng DSLR với khoản chi phí chấp nhận và bốn màu sắc hợp thời trang. Chiếc máy được trang bị một màn hình LCD TFT 2,7 inch góc ngắm rộng với 230.000 điểm ảnh, với được thiết kế bố trí lại để hỗ trợ chụp một tay. Tính năng Tự động lựa cảnh thông minh, Basic+ để tăng hiệu quả màu sắc và Chức năng hướng dẫn của máy sẽ giúp bạn hiểu rõ các tính năng hiệu quả của EOS 1100D một cách nhanh chóng.\r\nHệ AF chéo 9 điểm\r\nISO 100-3200, có thể mở rộng tới 6400\r\nThiết bị cảm biến CMOS cỡ APS-C 12.2 megapixels', 'http://trungviethung.com/Image/Picture/Canon/May_anh/1100D.jpg'),
(5, 'Máy ảnh', 'Máy ảnh Canon EOS 700D', 1000000, 'Hệ thống tự động lấy nét lai Hybrid AF\r\nLà mẫu EOS đầu tiên sử dụng hệ thống lấy nét tự động lai Hybrid AF, Canon EOS 700D cho phép kết hợp giữa lấy nét theo pha 9 điểm (Phase Detection) và lấy nét theo độ tương phản (Contract AF), giúp cho việc lấy nét cực nhanh với độ chính xác cao, ngay cả trong trường hợp thiếu sáng hoặc chủ thể ảnh đang di chuyển. Hệ thống lấy nét lai này đặc biệt hữu dụng trong chế độ Live View khi gương lật gập xuống và lấy nét theo pha không hoạt động được. ', 'https://hc.com.vn/media/catalog/product/cache/4/image/340x340/9df78eab33525d08d6e5fb8d27136e95/c/a/canon-eos-700d-anh-1.jpg'),
(6, 'Máy ảnh', 'Canon 500D (Body)', 1000000, ' Hãng sản xuất máy ảnh số 1 thế giới Canon vừa chính thức công bố máy ảnh Canon 500D, chiếc DSLR bình dân với khả năng quay video HD 1080p cùng với cảm biến ảnh hàng khủng 15 Mpixels. - Như vậy Canon 500D sẽ là chiếc DSLR thứ 3 trên thị trường với khả năng quay video độ phân giài cao: Với  Canon 500D, hãng Canon mong muốn đem lại cho khách hàng những trãi nghiệm chất lượng ảnh của dòng DSLR mà còn những trãi nghiệm video tương đương các dòng máy quay chuyên nghiệp. - Mặc dù là chiếc máy ảnh thuộc dòng phổ thông (entry-level), nhưng Canon 500D sở hữu những tính năng giống như dòng chuyên nghiệp. Máy cho chất lượng ảnh tốt hơn bằng bộ xử lý hình ảnh DIGIC 4 14 bit, tốc độ chụp ảnh 3,4 khung hình/giây, cảm biến 15,1 Megapixel, quay phim HD lên tới 1080p. - Nếu như 450D đi trước có ISO cao nhất chỉ 1600 thì Canon 500D cho phép mở rộng lên tới 6400 và 12800, 9 điểm lấy nét. Sau EOS 5D Mark II, đây là chiếc máy ảnh DSLR tiếp theo của Canon cho phép quay phim HD, với chế độ full HD 1.920 x 1.80 pixel, 20 khung hình/giây (12 phút); 1.280 x 720 pixel, 30 khung hình/giây (18 phút). - Canon cho biết, tại thị trường châu Á, Canon 500D sẽ có mặt vào đầu tháng 5, giá bán chỉ riêng body là 800 USD, ống kit đi kèm 18-55mm f/3.5-45.6 IS là 100 USD. - Bên cạnh mẫu máy ảnh trên, hãng còn ra mắt chiếc đèn flash Speedlight 270EX (bản nâng cấp của 220EX), hỗ trợ đánh bounce từ 0 lên 90 độ. ', 'https://mayanh24h.com/image/cache/catalog/product/0/0/000-350x350.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

DROP TABLE IF EXISTS `tbl_comment`;
CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `PD_ID` int(255) NOT NULL,
  `parent_comment_id` int(11) NOT NULL,
  `comment` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comment_sender_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`comment_id`, `PD_ID`, `parent_comment_id`, `comment`, `comment_sender_name`, `date`) VALUES
(9, 2, 0, 'aÌdasdsa', 'LÃª QuÃ´Ìc TiÃªÌn', '2019-01-16 09:56:03'),
(10, 1, 0, 'sdsdfsdfsdf', 'LÃª QuÃ´Ìc TiÃªÌn', '2019-01-16 14:20:02'),
(11, 1, 0, 'aÌdasdsad', 'LÃª QuÃ´Ìc TiÃªÌn', '2019-01-17 06:54:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
