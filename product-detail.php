<?php
  session_start();
 include 'data_access_helper.php';
 ?> 

<?php
//Insert to database
$db = new DataAccessHelper;
$db->connect();
mysqli_set_charset($conn, 'UTF8');

  $PD_ID = $_GET['PD_ID'];
  

  $query = mysqli_query($conn,"SELECT * FROM `product` WHERE `PD_ID` LIKE '%$PD_ID%'");





$db->close();
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="text/css" href="img/favicon.ico">

    <title>Aladdin - Sàn đấu giá sản phẩm tốt</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>

    <!-- Custom styles for this template -->
    <link type="text/css" href="css/header.css" rel="stylesheet">
    <link type="text/css" href="css/rating.css" rel="stylesheet">
    <link type="text/css" href="css/product-detail.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="css/style.css" />


  </head>
  <body>


    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="homepage.php"><img src="img/brand-logo.png" width="220" height="78px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

    <!-- Search -->
    <div id="main" >   
      <div class="main-search">   
          <div class="input-group">
            <form class="form-inline" action="Search.php" method="GET">

              <input type="search"   class="form-control " placeholder="Tìm kiếm..." name="q">
              <div class="input-group-btn">
              <button class="btn btn-default" type="submit" name="search" ><i class="fas fa-search"></i></button>
              </div>              
            </form>
          </div>  
          <br>
        <!-- Grid system -->
        <div id="search-result" class="row"> 
        </div>
      </div>    
    </div>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="shopping_cart.php"><i class="fas fa-shopping-cart"></i> Giỏ hàng
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="Myauction.php"> Đấu giá của tôi <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fas fa-user"></i> Tài khoản của tôi</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    

    <!-- Page Content -->
    <div style="margin-top: 2%" class="container">

      <div class="row"  style="padding-top: 2%">
        <?php

        $current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

         while ($row = mysqli_fetch_object($query)) { ?>
        <div class="col-lg-4">
            <div class="card h-100">
              <?php 
                
                echo " <img class='card-img-top' src = '$row->Image_link' />";
              ?>      
            </div>
          
        </div>
          <!-- /.col-lg-6  -->
              
              <div class="col-lg-8" >
                <div class="card-body" style="bottom :10%">
                  <h4 class="card-title" style="color: black">
                    <?php echo $row->Name; ?>
                  </h4>

                  <?php
                      echo '<h6 class="card-title" style="color: black"> Giá hiện tại: ' .$row->Price.' </h6><br>';
                      ?>

                  
                  <div class="countdown">
                  <h6 class="card-title" style="color: black">  Kết thúc trong: </h6>
                    <div class='tiles'></div>
                  </div>
                  <p class="card-text">
                  </p>

                <h6 class="card-title" style="color: black">Hãy là người đấu giá đầu tiên</h6><br>

                <h6 class="card-title" style="color: black">
                  <p id="textarea" style="color: black"><?php echo $row->Content; ?></p>
                  

                
                <form class="form-inline" method="post"  action="Myauction.php" >
                <div class="container"> 
                  <div class="row"> 
                    <div class="col-md-5 col-md-offset-3"> 
                      <div class="center">
                        <div class="input-group" >
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant">
                                <i class="fa fa-minus"></i>
                              </button>
                            </span>
                      <?php
                            echo '<input type="text" name="quant" class="form-control input-number"  min="1000" max="100000000" value = ' .$row->Price += '4000'.' >';     
                      ?>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant">
                                <i class="fa fa-plus"></i>
                                </button>
                            </span>
                            <button type="submit" class="add" name="auction" onclick='javascript: return SubmitForm()'> Đấu giá ngay</button>
                        </div>
                        <?php
                        
                        echo '<input type="hidden" name="PD_ID" value="'.$row->PD_ID.'" />';
                        echo '<input type="hidden" name="type" value="add" />';
                        echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
                        
                        ?>
                        
                      </div>
                    </div>
                  </div>
                </div>
                   
                
                </form>
                </div>
              </div>
              
              <div class="col-md-12">
            <div class="product-tab">

              <div class="tabs">
                <ul class="tab-links">
                  <li class="active"><a href="#tab1">Description</a></li>
                  <li><a href="#tab2">Details</a></li>
                  <li><a href="#tab3">Reviews</a></li>
                  
                </ul>

              <div class="tab-content">
                <div id="tab1" class="tab active" style="padding-top: 10px">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                      irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div id="tab2" class="tab" style="padding-top: 10px">
                    <p>TLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                      irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div id="tab3"class="tab" style="padding-top: 10px">

                  <div class="row">
                    <div class="col-md-6">
                      <div class="product-reviews">
                        <span id="comment_message"></span>
                         <br />
                         <div id="display_comment"></div>

                      </div>
                    </div>
                    <div class="col-md-6">
                      <h4 class="text-uppercase">Write Your Review</h4>
                      
                      <form class="review-form" method="POST" id="comment_form" action="fetch_comment.php">
                        <div class="form-group">
                          <input type="text" name="comment_name" id="comment_name" class="input" placeholder="Your Name" />
                        </div>
                        
                        <div class="form-group">
                          <textarea name="comment_content" id="comment_content" class="input" placeholder="Enter Comment" ></textarea>
                        </div>
                        
                        <div>
                          <input type="hidden" name="comment_id" id="comment_id" value="0" />
                          <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />
                        <?php  echo '<input type="hidden" name="PD_ID" value="'.$row->PD_ID.'" />';?>
                        </div>

                      </form>
                    </div>
                      
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
              <?php } ?>
              <!-- /.col-lg-6  -->
            </div>
              <!-- /.row  -->
          </div>
          <!-- /.container  -->

    <!-- Footer -->
    <footer class="py-5">
      <div class="container">
        <div class="row">

          <div class="col-lg-4">
            <h4 class="my-4">Về Aladdin</h4>
            <br><a href="#">Về chúng tôi</a></br>
            <br><a href="#">Điều khoản sử dụng</a></br>
            <br><a href="#">Chính sách bảo mật</a></br>
            <br><a href="#">Thông tin công ty</a></br>
            <br><a href="#">Tuyển dụng</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Trợ Giúp & Liên Hệ</h4>
            <br><a href="#">Liên hệ</a></br>
            <br><a href="#">Thông tin giao hàng</a></br>
            <br><a href="#">Đăng ký</a></br>
            <br><a href="#">Trợ giúp</a></br>
            <br><a href="#">Thanh toán & Vận chuyển</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Liên Kết</h4>
            <br><a href="#"><i class="fab fa-facebook"></i> Facebook</a></br>
            <br><a href="#"><i class="fab fa-twitter"></i> Twitter</a></br>
            <br><a href="#"><i class="fab fa-instagram"></i> Instagram</a></br>
            <br><a href="#"><i class="fab fa-youtube"></i> Youtube</a></br>
          </div>

      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/clock.js" ></script>
    <script type="text/javascript" src="js/box-auction.js" ></script>
    <script src="js/tab.js"></script>
    <script>
    $(document).ready(function(){
     
     $('#comment_form').on('submit', function(event){
      event.preventDefault();
      var form_data = $(this).serialize();
      console.log(form_data);
      $.ajax({
       url:"add_comment.php",
       method:"POST",
       data:form_data,
       dataType:"JSON",
       success:function(data)
       {
        if(data.error != '')
        {
         $('#comment_form')[0].reset();
         $('#comment_message').html(data.error);
         $('#comment_id').val('0');
         load_comment();
        }
       }
      })
     });
     load_comment();

     function load_comment()
     {
      $.ajax({
       url:"fetch_comment.php",
       method:"POST",
       
       success:function(data)
       {
        $('#display_comment').html(data);
       }
      })
     }

     $(document).on('click', '.reply', function(){
      var comment_id = $(this).attr("id");
      $('#comment_id').val(comment_id);
      $('#comment_name').focus();
     });
     
    });
    </script>


    <!-- <script type="text/javascript" src="thử.js" ></script> -->
    <script type="text/javascript">
      $('document').ready(function() {
        $('.carousel').carousel();
      })
    </script>


  </body>
</html>

