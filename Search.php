<?php
 include 'data_access_helper.php';
 ?> 

<?php
//Insert to database
$db = new DataAccessHelper;
$db->connect();
mysqli_set_charset($conn, 'UTF8');
if (isset($_GET['search'])) {    
  $q = $_GET['q'];
  $query = mysqli_query($conn,"SELECT * FROM `product` WHERE `Name` LIKE '%$q%'");
  $count = mysqli_num_rows($query);

}
  if (empty($q)){
    header('location: homepage.php');
  }

$result = mysqli_query($conn,"SELECT Category FROM `product` GROUP BY Category ");

$db->close();
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="text/css" href="img/favicon.ico">

    <title>Aladdin - Sàn đấu giá sản phẩm tốt</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>

    <!-- Custom styles for this template -->
    <link type="text/css" href="css/header.css" rel="stylesheet">
    <link type="text/css" href="css/rating.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  </head>
  <body>


    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="homepage.php"><img src="img/brand-logo.png" width="220" height="78px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

    <!-- Search -->
    <div id="main" >   
      <div class="main-search">   
          <div class="input-group">
            <form class="form-inline" action="Search.php" method="GET">

              <input type="search"   class="form-control " placeholder="Tìm kiếm..." name="q">
              <div class="input-group-btn">
              <button class="btn btn-default" type="submit" name="search" ><i class="fas fa-search"></i></button>
              </div>              
            </form>
          </div>  
          <br>
        <!-- Grid system -->
        <div id="search-result" class="row"> 
        </div>
      </div>    
    </div>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i> Giỏ hàng
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"> Đấu giá của tôi <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fas fa-user"></i> Tài khoản của tôi</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    

    <!-- Page Content -->
    <div style="margin-top: 2%" class="container">

      <div class="row">

        <div class="col-lg-3">

          <h1 class="my-4">Shop Name</h1>
          <div class="list-group"><b>
            <?php while ($row = mysqli_fetch_object($result)){
              $Cat = $row->Category;
              echo "<a href = 'Category.php?Category=$Cat' class='list-group-item'>" . "$row->Category" . "</a>";
            }
            ?>
            
          </b>
          </div>

        </div>
        <!-- /.col-lg-3  -->

        <div class="col-lg-9" >
          <h4 class="card-title" style="color: black; margin-top: 5%;"> Kết quả tìm kiếm cho '<?php echo "$q"; ?>' : <?php echo $count; ?></h4>
            <div class="row">

              <?php while ($row = mysqli_fetch_object($query)) { ?>
              <div class="col-lg-4 col-md-6 mb-4">
                <div class="card-body">
                  <?php
                  $PD_ID = $row->PD_ID;
                    echo "<a href = 'product-detail.php?PD_ID=$PD_ID' >" . " <img class='card-img-top' src = '$row->Image_link' />" . "</a>";
                  ?>
                  <h4 class="card-title" style="color: black">
                    <?php echo $row->Name; ?>
                  </h4>

                  <h6 class="card-title" style="color: black"> Giá hiện tại: <?php echo $row->Price; ?> </h6><br>
                  
                  <div class="countdown">
                  <h6 class="card-title" style="color: black">  Kết thúc trong: </h6>
                    <div class='tiles'></div>
                  </div>
                  <p class="card-text">
                  </p>

                </div>
              </div>
              
              <?php } ?>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.col-lg-9 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5">
      <div class="container">
        <div class="row">

          <div class="col-lg-4">
            <h4 class="my-4">Về Aladdin</h4>
            <br><a href="#">Về chúng tôi</a></br>
            <br><a href="#">Điều khoản sử dụng</a></br>
            <br><a href="#">Chính sách bảo mật</a></br>
            <br><a href="#">Thông tin công ty</a></br>
            <br><a href="#">Tuyển dụng</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Trợ Giúp & Liên Hệ</h4>
            <br><a href="#">Liên hệ</a></br>
            <br><a href="#">Thông tin giao hàng</a></br>
            <br><a href="#">Đăng ký</a></br>
            <br><a href="#">Trợ giúp</a></br>
            <br><a href="#">Thanh toán & Vận chuyển</a></br>
          </div>

          <div class="col-lg-4">
            <h4 class="my-4">Liên Kết</h4>
            <br><a href="#"><i class="fab fa-facebook"></i> Facebook</a></br>
            <br><a href="#"><i class="fab fa-twitter"></i> Twitter</a></br>
            <br><a href="#"><i class="fab fa-instagram"></i> Instagram</a></br>
            <br><a href="#"><i class="fab fa-youtube"></i> Youtube</a></br>
          </div>

      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="E:\học tập\Web\đồ án\css\jquery\jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/clock.js" ></script>
    <script type="text/javascript" src="js/box-price.js" ></script>
    
    <!-- <script type="text/javascript" src="thử.js" ></script> -->
    <script type="text/javascript">
      $('document').ready(function() {
        $('.carousel').carousel();
      })
    </script>


  </body>
</html>

